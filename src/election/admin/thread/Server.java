package election.admin.thread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import election.admin.Main;

public class Server extends Thread {
	
	private ServerSocket socket = null;
	private Callbacks callbacks = null;
	
	private int port;
	
	public Server(Main election, int port) {
		callbacks = (Callbacks)election;
		this.port = port;
	}
	
	public void run() {
		System.out.println("server start");
		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while (true) {
			try {
				Socket client = socket.accept();
				callbacks.startClient(client);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static interface Callbacks {
		void startClient(Socket client);
	}
}