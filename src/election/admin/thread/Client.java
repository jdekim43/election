package election.admin.thread;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;

import election.admin.Main;
import election.data.Candidator;
import election.data.Voter;
import election.util.Form;
import election.util.FormCreator;

public class Client extends Thread {
	
	private Callbacks callbacks = null;
	private Socket socket = null;
	private ObjectInputStream receiver = null;
	
	private boolean isRun = true;
	
	public Client(Main election, Socket socket) {
		System.out.println("start client"+socket);
		callbacks = (Callbacks)election;
		this.socket = socket;
		try {
			callbacks.setSender(this, new ObjectOutputStream(socket.getOutputStream()));
			receiver = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		while(isRun) {
			Object in = null;
			try {
				in = receiver.readObject();
				System.out.println(in);
			} catch(IOException e) {
				//e.printStackTrace();
			} catch(ClassNotFoundException e) {
				//e.printStackTrace();
			}
			if (in == null) {
				continue;
			}
			Form input = null;
			try {
				input = (Form)in;
			} catch(ClassCastException e) {
				e.printStackTrace();
			}
			String type = input.type;
			HashMap<String, Object> data = input.data;
			if (type.equals(FormCreator.LOGIN)) {
				System.out.println("client login callback");
				callbacks.validUser(this, (String)data.get("id"), (String)data.get("pw"));
			}
			else if (type.equals(FormCreator.REQUEST_DATA)) {
				if (data.get("request_data").equals("election_info")) {
					callbacks.sendElectionInfo(this);
				}
			}
			else if (type.equals(FormCreator.VOTE)) {
				callbacks.vote((Voter)data.get("voter"), (Candidator)data.get("candidator"));
				System.out.println("receive voter "+data.get("voter"));
			}
			else if (type.equals(FormCreator.QUIT)) {
				isRun = false;
				callbacks.endClient(this);
				continue;
			}
		}
		try {
			socket.close();
		} catch(IOException e) {
			
		}
		this.interrupt();
	}
	
	public static interface Callbacks {
		void setSender(Client client, ObjectOutputStream sender);
		void validUser(Client client, String id, String pw);
		void sendElectionInfo(Client client);
		void vote(Voter voter, Candidator cnadidator);
		void endClient(Client client);
	}
}