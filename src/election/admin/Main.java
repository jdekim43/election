package election.admin;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import election.admin.thread.Client;
import election.admin.thread.Server;
import election.admin.view.ElectionSetting;
import election.admin.view.Electioning;
import election.admin.view.Ended;
import election.data.Candidator;
import election.data.Election;
import election.data.Voter;
import election.data.VoterTableModel;
import election.util.Form;
import election.util.FormCreator;

public class Main implements ElectionSetting.Callbacks, Electioning.Callbacks, Ended.Callbacks, Server.Callbacks, Client.Callbacks {
	
	private static ElectionSetting settingView = null;
	private static Electioning electioningView = null;
	private static Ended endedView = null;
	
	private static ArrayList<Candidator> votedList = null;
	
	private Server server = null;
	private HashMap<Client, ObjectOutputStream> clients = null;
	
	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		init();
		settingView.open();
	}
	
	private void serverStart() {
		server.start();
	}
	
	private void init() {
		settingView = new ElectionSetting(this);
		electioningView = new Electioning(this);
		endedView = new Ended(this);
		votedList = new ArrayList<Candidator>();
		server = new Server(this, Election.PORT);
		clients = new HashMap<Client, ObjectOutputStream>();
	}
	
	private void send(Client client, Form msg) {
		System.out.println(msg);
		synchronized(clients) {
			ObjectOutputStream sender = clients.get(client);
			try {
				sender.writeObject(msg);
				sender.flush();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}

	@Override
	public void addVoter(Voter voter) {
		//Election.voterList.addElement(voter);
		Election.voterList.add(voter);
	}

	@Override
	public void addVoterFromCSV(String path) {
		String fileData = "";
		try {
			fileData = new String(Files.readAllBytes(Paths.get(path)));
		}
		catch (IOException e) {
			System.out.println(e);
			System.exit(1);
		}
		if (fileData == null || fileData.equals("")) {
			return;
		}
		String[] voters = fileData.split("\n");
		for (int i=1; i<voters.length; i++) {
			String[] voterInfo = voters[i].split(",");
			addVoter(new Voter(voterInfo[0].trim(), voterInfo[1].trim(), voterInfo[2].trim()));
		}
	}

	@Override
	public void addCandidator(Candidator candidator) {
		Election.candidatorList.add(candidator);
	}

	@Override
	public void setElection(String title, ImageIcon logo) {
		Election.title = title;
		Election.logo = logo;
	}

	@Override
	public void startElection() {
		if (Election.title==null || Election.logo==null) {
			JOptionPane.showMessageDialog(null, "정보를 모두 입력하세요.");
			return;
		}
		if (Election.candidatorList == null || Election.candidatorList.getSize()<=0) {
			JOptionPane.showMessageDialog(null, "적어도 한명 이상의 후보가 있어야 합니다.");
			return;
		}
		electioningView.open();
		settingView.close();
		serverStart();
	}

	@Override
	public void endElection() {
		for (int i=0; i<Election.voterList.getSize(); i++) {
			if (!Election.voterList.get(i).isVoted()) {
				int ans = JOptionPane.showConfirmDialog(null, "투표를 하지 않은 유권자가 있습니다. 그래도 선거를 종료하시겠습니까?");
				if (ans == JOptionPane.OK_OPTION) {
					endedView.open();
					endedView.counting(votedList);
					electioningView.close();
					server.interrupt();
					System.out.println("server end");
				}
				return;
			}
		}
		int ans = JOptionPane.showConfirmDialog(null, "선거를 종료하시겠습니까?");
		if (ans == JOptionPane.OK_OPTION) {
			endedView.open();
			endedView.counting(votedList);
			electioningView.close();
			server.interrupt();
			System.out.println("server end");
		}
	}

	@Override
	public void startClient(Socket client) {
		Client clientThread = new Client(this, client);
		clientThread.start();
	}

	@Override
	public void setSender(Client client, ObjectOutputStream sender) {
		clients.put(client, sender);
	}

	@Override
	public void validUser(Client client, String id, String pw) {
		System.out.println("validUser");
		for (int i=0; i<Election.voterList.getSize(); i++) {
			if (Election.voterList.get(i).isValid(id, pw) && !Election.voterList.get(i).isVoted()) {
				System.out.println("valid"+Election.voterList.get(i).isValid(id, pw)+" | "+!Election.voterList.get(i).isVoted());
				send(client, FormCreator.loginResult(Election.voterList.get(i)));
				return;
			}
		}
		send(client, FormCreator.loginResult(Election.LOGIN_ERROR));
	}

	@Override
	public void sendElectionInfo(Client client) {
		send(client, FormCreator.electionInfo(Election.title, Election.logo, Election.candidatorList));
	}

	@Override
	public void vote(Voter voter, Candidator candidator) {
		for (int i=0; i<Election.voterList.getSize(); i++) {
			if (Election.voterList.get(i).getNumber().equals(voter.getNumber())) {
				//Election.voterList.get(i).setVoted();
				Election.voterList.setValueAt(true, i, 2);
				votedList.add(candidator);
				electioningView.addVotedVoter(Election.voterList.get(i));
				System.out.println("voted");
			}
		}
		System.out.println("vote method voted "+candidator.name);
	}

	@Override
	public void endClient(Client client) {
		synchronized(clients) {
			clients.remove(client);
		}
		client.interrupt();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void open(String path) {
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
			
			Election.title = (String)ois.readObject();
			Election.logo = (ImageIcon)ois.readObject();
			Vector candidatorListData = (Vector)ois.readObject();
			for (int i=0; i<candidatorListData.size(); i++) {
				Election.candidatorList.addRow((Vector)candidatorListData.get(i));
			}
			Vector voterListData = (Vector)ois.readObject();
			for (int i=0; i<voterListData.size(); i++) {
				Election.voterList.addRow((Vector)voterListData.get(i));
			}

			ois.close();
		} catch(Exception e){
		}
		settingView.close();
		settingView = null;
		settingView = new ElectionSetting(this);
		settingView.open();
	}

	@Override
	public void saveElection(String path) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
			
			oos.writeObject(Election.title);
			oos.flush();
			oos.writeObject(Election.logo);
			oos.flush();
			oos.writeObject(Election.candidatorList.getDataVector());
			oos.flush();
			oos.writeObject(Election.voterList.getDataVector());
			oos.flush();
			
			oos.close();
		} catch(Exception e) {
			
		}
	}

	@Override
	public void saveResult(String path) {
		
	}
}