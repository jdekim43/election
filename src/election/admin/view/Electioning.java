package election.admin.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import election.admin.Main;
import election.data.Election;
import election.data.Voter;
import election.data.VoterTableModel;

public class Electioning extends JFrame {
	
	private Callbacks callbacks = null;
	
	private VoterTableModel notVotedVoterList = null;
	private VoterTableModel votedVoterList = null;
	
	public Electioning(Main election) {
		callbacks = (Callbacks)election;
		init();
		setView();
	}
	
	public void open() {
		for (int i=0; i<Election.voterList.getSize(); i++) {
			System.out.println(i);
			if (Election.voterList.get(i).isVoted()) {
				votedVoterList.add(Election.voterList.get(i));
			}
			else {
				notVotedVoterList.add(Election.voterList.get(i));
			}
		}
		setVisible(true);
	}
	
	public void close() {
		setVisible(false);
	}
	
	public void addVotedVoter(Voter voter) {
		notVotedVoterList.remove(voter);
		votedVoterList.add(voter);
		System.out.println("voted "+voter);
	}
	
	private void init() {
		notVotedVoterList = new VoterTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		votedVoterList = new VoterTableModel(){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}
	
	private void setView() {
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		/*voter list container*/
		JTabbedPane mainPane = new JTabbedPane();
		getContentPane().add(mainPane, BorderLayout.CENTER);
		
		/*not voted voter list*/
		JTable notVotedListView = new JTable();
		notVotedListView.setModel(notVotedVoterList);
		mainPane.add("미투표자 목록", new JScrollPane(notVotedListView));
		
		/*votd voter list*/
		JTable votedListView = new JTable();
		votedListView.setModel(votedVoterList);
		mainPane.add("투표자 목록", new JScrollPane(votedListView));
		
		JLabel percent = new JLabel("투표 진행률 : 0%");
		percent.setFont(new Font("Arial", Font.BOLD, 20));
		votedVoterList.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				float notVotedNum = notVotedVoterList.getSize();
				float votedNum = votedVoterList.getSize();
				percent.setText("투표 진행률 : "+((votedNum/(votedNum+notVotedNum))*100)+"%");
			}
		});
		getContentPane().add(percent, BorderLayout.NORTH);
		
		JButton endBtn = new JButton("선거 종료");
		endBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callbacks.endElection();
			}
		});
		getContentPane().add(endBtn, BorderLayout.SOUTH);
		
		setSize(800, 600);
	}
	
	public static interface Callbacks {
		void endElection();
	}
}