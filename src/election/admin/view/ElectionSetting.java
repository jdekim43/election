package election.admin.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import election.admin.Main;
import election.data.Candidator;
import election.data.Election;
import election.data.Voter;

public class ElectionSetting extends JFrame {
	
	private Callbacks callbacks = null;
	
	private JLabel voterCount = null;
	private JLabel candidatorCount = null;
	
	private TextField inputElectionName = null;
	private ImageIcon logoImage = null;
	
	public ElectionSetting(Main election) {
		callbacks = (Callbacks)election;
		setView();
	}
	
	public void open() {
		setVisible(true);
	}
	
	public void close() {
		setVisible(false);
	}
	
	private void setView() {
		MenuBar menuBar = new MenuBar();
		Menu fileMenu = new Menu("파일");
		MenuItem openElectionMenu = new MenuItem("선거 열기");
		MenuItem saveElectionMenu = new MenuItem("선거 저장");
		/*
		Menu voterMenu = new Menu("유권자 관리");
		MenuItem addVoterMenu = new MenuItem("유권자 등록");
		MenuItem viewVoterMenu = new MenuItem("등록된 유권자 보기");
		Menu candidatorMenu = new Menu("후보 관리");
		MenuItem addCandidatorMenu = new MenuItem("후보 등록");
		MenuItem viewCandidatorMenu = new MenuItem("등록된 후보 보기");
		*/
		fileMenu.add(openElectionMenu);
		fileMenu.add(saveElectionMenu);
		/*
		voterMenu.add(addVoterMenu);
		voterMenu.add(viewVoterMenu);
		candidatorMenu.add(addCandidatorMenu);
		candidatorMenu.add(viewCandidatorMenu);
		*/
		menuBar.add(fileMenu);
		/*
		menuBar.add(voterMenu);
		menuBar.add(candidatorMenu);
		*/
		setMenuBar(menuBar);
		
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("선거 설정 파일", "jke"));
		
		openElectionMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int ans = fc.showOpenDialog(null);
				if (ans==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					callbacks.open(file.getAbsolutePath());
				}
			}
		});
		saveElectionMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Election.title = inputElectionName.getText();
				Election.logo = logoImage;
				int ans = fc.showSaveDialog(null);
				if (ans==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					if (file.getName().endsWith(".jke")) {
						callbacks.saveElection(file.getAbsolutePath());
					} else {
						callbacks.saveElection(file.getAbsolutePath()+".jke");
					}
				}
			}
		});
		
		getContentPane().setLayout(new BorderLayout(3, 3));
		
		/*선거 관리 / 유권자 관리 / 후보 관리*/
		JTabbedPane mainPane = new JTabbedPane();
		
		mainPane.add("선거 관리", createElectionAdminView());
		
		mainPane.add("유권자 관리", createVoterAdminView());
		
		mainPane.add("후보 관리", createCandidatorAdminView());
		
		getContentPane().add(mainPane, BorderLayout.CENTER);
		
		JButton voteStartBtn = new JButton("선거 시작");
		voteStartBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callbacks.setElection(inputElectionName.getText(), logoImage);
				callbacks.startElection();
			}
		});
		
		getContentPane().add(voteStartBtn, BorderLayout.SOUTH);
		
		setSize(800, 600);
	}
	
	private JPanel createElectionAdminView() {
		JPanel container = new JPanel();
		JPanel inputForm = new JPanel();
		JPanel nameForm = new JPanel();
		JPanel logoForm = new JPanel();
		inputElectionName = new TextField();
		inputElectionName.setText(Election.title);
		System.out.println(Election.title);
		
		container.setLayout(new GridBagLayout());
		inputForm.setLayout(new BoxLayout(inputForm, BoxLayout.Y_AXIS));
		nameForm.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));
		logoForm.setLayout(new BoxLayout(logoForm, BoxLayout.X_AXIS));
		
		inputElectionName.setPreferredSize(new Dimension(400, 30));
		
		nameForm.add(new JLabel("선거 이름"));
		nameForm.add(inputElectionName);
		inputForm.add(nameForm);
		logoForm.add(new JLabel("로고"));
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("이미지 파일", "png", "jpg", "jpeg", "bmp"));
		JButton selectBtn = new JButton("이미지 선택");
		logoImage = Election.logo;
		if (logoImage == null) logoImage = new ImageIcon();
		selectBtn.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				int ans = fc.showOpenDialog(null);
				if (ans==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					logoImage.setImage(Toolkit.getDefaultToolkit().getImage(file.getAbsolutePath()));
					logoForm.updateUI();
				}
			}
		});
		logoForm.add(selectBtn);
		logoForm.add(new JLabel(logoImage));
		inputForm.add(logoForm);
		container.add(inputForm);
		
		return container;
	}
	
	private JPanel createVoterAdminView() {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout(3, 3));
		
		JPanel top = new JPanel();
		top.setLayout(new GridLayout(1, 3, 4, 4));
		voterCount = new JLabel("현재 "+Election.voterList.getSize()+"명의 유권자가 등록되어 있습니다.");
		top.add(voterCount);
		
		JButton csvAddBtn = new JButton("유권자 목록 csv로 불러오기");
		csvAddBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				fc.setFileFilter(new FileNameExtensionFilter("쉼표로 구분된 csv 파일", "csv"));
				int ans = fc.showOpenDialog(null);
				if (ans == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					callbacks.addVoterFromCSV(file.getAbsolutePath());
					voterCount.setText("현재 "+Election.voterList.getSize()+"명의 유권자가 등록되어 있습니다.");
				}
			}
		});
		top.add(csvAddBtn);
		
		JButton voterAddBtn = new JButton("유권자 추가");
		
		final JTextField numberField = new JTextField();
		final JTextField nameField = new JTextField();
		final JTextField codeField = new JTextField();
		JComponent[] inputs = new JComponent[] {
				new JLabel("학번"),
				numberField,
				new JLabel("이름"),
				nameField,
				new JLabel("코드"),
				codeField
		};
		voterAddBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				numberField.setText("");
				nameField.setText("");
				codeField.setText("");
				int ans = JOptionPane.showOptionDialog(null, inputs, "유권자 추가", JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				if (ans == JOptionPane.OK_OPTION) {
					callbacks.addVoter(new Voter(numberField.getText(), nameField.getText(), codeField.getText()));
					voterCount.setText("현재 "+Election.voterList.getSize()+"명의 유권자가 등록되었습니다.");
				}
			}
		});
		top.add(voterAddBtn);
		
		JTable votersView = new JTable();
		votersView.setModel(Election.voterList);
		
		container.add(top, BorderLayout.NORTH);
		container.add(new JScrollPane(votersView), BorderLayout.CENTER);
		
		return container;
	}

	ImageIcon pledgeImage = null;
	private JPanel createCandidatorAdminView() {
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout(3, 3));
		
		JPanel top = new JPanel();
		top.setLayout(new GridLayout(1, 3, 4, 4));
		candidatorCount = new JLabel("현재 "+Election.candidatorList.getSize()+"명의 후보가 등록되어 있습니다.");
		top.add(candidatorCount);
		JButton candidatorAddBtn = new JButton("후보 추가");
		
		JTextField numberField = new JTextField();
		JTextField nameField = new JTextField();
		final JFileChooser fc = new JFileChooser();
		final JLabel pledgeImageLabel = new JLabel("이미지");
		JButton pledgeSelectBtn = new JButton("이미지 선택");
		pledgeSelectBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int ans = fc.showOpenDialog(null);
				if (ans==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					pledgeImage = new ImageIcon();
					pledgeImage.setImage(Toolkit.getDefaultToolkit().getImage(file.getAbsolutePath()));
				}
			}
		});
		JComponent[] inputs = new JComponent[] {
				new JLabel("기호 (숫자만 입력)"),
				numberField,
				new JLabel("이름"),
				nameField,
				pledgeImageLabel,
				pledgeSelectBtn
		};
		
		candidatorAddBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				numberField.setText("");
				nameField.setText("");
				int ans = JOptionPane.showOptionDialog(null, inputs, "후보 추가", JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				if (ans == JOptionPane.OK_OPTION) {
					callbacks.addCandidator(new Candidator(Integer.parseInt(numberField.getText()), nameField.getText(), pledgeImage));
					candidatorCount.setText("현재 "+Election.candidatorList.getSize()+"명의 후보가 등록되었습니다.");
				}
			}
		});
		top.add(candidatorAddBtn);

		JTable candidatorsView = new JTable();
		candidatorsView.setModel(Election.candidatorList);
		
		container.add(top, BorderLayout.NORTH);
		container.add(new JScrollPane(candidatorsView), BorderLayout.CENTER);
		
		return container;
	}
	
	public static interface Callbacks {
		void addVoter(Voter voter);
		void addVoterFromCSV(String path);
		void addCandidator(Candidator candidator);
		void setElection(String title, ImageIcon logo);
		void startElection();
		void open(String path);
		void saveElection(String path);
	}
}