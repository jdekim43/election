package election.admin.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

import election.admin.Main;
import election.data.Candidator;
import election.data.Election;

public class Ended extends JFrame {
	
	DefaultCategoryDataset dataset = null;
	
	public Ended(Main electino) {
		init();
		setView();
	}
	
	public void open() {
		setVisible(true);
	}
	
	public void close() {
		setVisible(false);
	}
	
	public void counting(ArrayList<Candidator> list) {
		for (int i=0; i<Election.candidatorList.getSize(); i++) {
			dataset.addValue(0, Election.title, Election.candidatorList.get(i).name);
		}
		dataset.addValue(0, Election.title, Election.ABSTENTION.name);
		for (int i=0; i<list.size(); i++) {
			dataset.addValue((double)dataset.getValue(Election.title, list.get(i).name)+1, Election.title, list.get(i).name);
		}
		System.out.println(Election.candidatorList.getSize() + list.size());
	}
	
	private void init() {
		dataset = new DefaultCategoryDataset();
	}
	
	private void setView() {
		getContentPane().setLayout(new BorderLayout(4, 4));
		
		getContentPane().add(new JLabel(Election.title), BorderLayout.NORTH);
		
		/*후보 별 득표 수 + 기권 수*/
		JFreeChart chart = createChart();
		ChartPanel container = new ChartPanel(chart);
		getContentPane().add(container, BorderLayout.CENTER);
		
		/*결과 저장하기*/
		JButton saveButton = new JButton("결과 저장");
		saveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				fc.setFileFilter(new FileNameExtensionFilter("이미지 파일", "png"));
				int ans = fc.showSaveDialog(null);
				if (ans==JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
				    
				    try{
				    	FileOutputStream fos=new FileOutputStream(file);
				    	ChartUtilities.writeChartAsPNG(fos, chart, 600, 600);
				    	fos.flush();
				    	fos.close();
				    } catch(Exception ex){
				    	ex.printStackTrace();
				    }
				}
			}
		});
		getContentPane().add(saveButton, BorderLayout.SOUTH);
		
		setSize(800, 600);
	}
	
	private JFreeChart createChart() {
		BarRenderer renderer = new BarRenderer();
		
		CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator();
		
		ItemLabelPosition p_center = new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER);
		
		renderer.setBaseItemLabelGenerator(generator);
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(p_center);
		
		CategoryPlot plot = new CategoryPlot();
		plot.setDataset(dataset);
		plot.setRenderer(renderer);
		plot.setOrientation(PlotOrientation.VERTICAL);
		plot.setRangeGridlinesVisible(true);
		plot.setDomainGridlinesVisible(true);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		plot.setDomainAxis(new CategoryAxis());
		plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
		plot.setRangeAxis(new NumberAxis());
		
		return new JFreeChart(plot);
	}
	
	//TODO: add data
	
	public static interface Callbacks {
		public void saveResult(String path);
	}
}