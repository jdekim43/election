package election.util;

import java.awt.Image;

import javax.swing.DefaultListModel;

import election.data.Candidator;
import election.data.Voter;
import election.util.json.JSONArray;
import election.util.json.JSONException;
import election.util.json.JSONObject;

public class MessageCreator {
	
	public static final String REQUEST_DATA = "request_data";
	public static final String LOGIN = "login";
	public static final String VOTE = "vote";
	public static final String ELECTION_INFO = "election_info";
	
	public static String requestData(String requestData) {
		try {
			JSONObject jo = new JSONObject();
			jo.put("type", REQUEST_DATA);
			jo.put("request_data", requestData);
			return jo.toString();
		} catch (JSONException e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static String vote(Voter voter, Candidator candidator) {
		try {
			JSONObject jo = new JSONObject();
			jo.put("type", VOTE);
			jo.put("voter", new JSONObject(voter));
			jo.put("candidator", new JSONObject(candidator));
			return jo.toString();
		} catch (JSONException e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static String login(String id, String pw) {
		try {
			JSONObject jo = new JSONObject();
			jo.put("type", LOGIN);
			jo.put("id", id);
			jo.put("pw", pw);
			return jo.toString();
		} catch(JSONException e) {
			System.out.println(e);
		}
		return null;
	}
	
	
	
	public static String loginResult(Voter voter) {
		try {
			JSONObject jo = new JSONObject();
			jo.put("type", LOGIN);
			jo.put("voter", new JSONObject(voter));
			return jo.toString();
		} catch(JSONException e) {
			System.out.println(e);
		}
		return null;
	}
	
	public static String electionInfo(String title, Image logo, DefaultListModel<Candidator> candidatorList) {
		try {
			JSONObject jo = new JSONObject();
			jo.put("type", ELECTION_INFO);
			jo.put("title", title);
			jo.put("logo", new JSONObject(logo));
			
			JSONArray ja = new JSONArray();
			for (int i=0; i<candidatorList.getSize(); i++) {
				ja.put(new JSONObject(candidatorList.get(i)));
			}
			jo.put("candidator_list", ja);
			
			return jo.toString();
		} catch(JSONException e) {
			System.out.println(e);
		}
		return null;
	}
}