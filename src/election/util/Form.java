package election.util;

import java.io.Serializable;
import java.util.HashMap;

public class Form implements Serializable {
	public String type;
	public HashMap<String, Object> data;
	public Form(String type, HashMap<String, Object> data) {
		this.type = type;
		this.data = data;
	}
}