package election.util;

import java.util.HashMap;

import javax.swing.ImageIcon;

import election.data.Candidator;
import election.data.CandidatorTableModel;
import election.data.Voter;

public class FormCreator {
	
	public static final String REQUEST_DATA = "request";
	public static final String LOGIN = "login";
	public static final String VOTE = "vote";
	public static final String ELECTION_INFO = "election_info";
	public static final String QUIT = "quit";
	
	public static Form requestData(String requestData) {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("request_data", requestData);
		return new Form(REQUEST_DATA, data);
	}
	
	public static Form vote(Voter voter, Candidator candidator) {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("voter", voter);
		data.put("candidator", candidator);
		return new Form(VOTE, data);
	}
	
	public static Form login(String id, String pw) {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("id", id);
		data.put("pw", pw);
		return new Form(LOGIN, data);
	}
	
	public static Form loginResult(Voter voter) {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("voter", voter);
		return new Form(LOGIN, data);
	}
	
	public static Form electionInfo(String title, ImageIcon logo, CandidatorTableModel candidatorList) {
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("title", title);
		data.put("logo", logo);
		data.put("candidator_list", candidatorList.getDataVector());
		return new Form(ELECTION_INFO, data);
	}
	
	public static Form quit() {
		HashMap<String, Object> data = new HashMap<String, Object>();
		return new Form(QUIT, data);
	}
}