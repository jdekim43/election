package election.data;

import java.io.Serializable;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

public class CandidatorTableModel extends DefaultTableModel implements Serializable {
	
	public CandidatorTableModel() {
		super.setColumnIdentifiers(new String[]{"기호", "이름", "이미지 파일"});
	}
	
	public void add(Candidator candidator) {
		for (int i=0; i<getSize(); i++) {
			if (candidator.num == get(i).num) {
				System.out.println("동일한 기호를 가진 후보는 추가할 수 없습니다.");
				return;
			}
		}
		super.addRow(new Object[]{candidator.num, candidator.name, candidator.pledge});
	}
	
	public int getSize() {
		return super.getRowCount();
	}
	
	public Candidator get(int index) {
		return new Candidator((int)super.getValueAt(index, 0), (String)super.getValueAt(index, 1), (ImageIcon)super.getValueAt(index, 2));
	}
	
	public DefaultListModel<Candidator> getCandidatorListModel() {
		DefaultListModel<Candidator> list = new DefaultListModel<Candidator>();
		for (int i=0; i<getSize(); i++) {
			list.addElement(get(i));
		}
		return list;
	}
	
	@Override
	public boolean isCellEditable(int row, int column){
		System.out.println(column);
		if (column == 2) {
			return false;
		}
		return true;
    }
}