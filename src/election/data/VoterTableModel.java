package election.data;

import java.io.Serializable;

import javax.swing.table.DefaultTableModel;

public class VoterTableModel extends DefaultTableModel implements Serializable {
	
	public VoterTableModel() {
		super.setColumnIdentifiers(new String[]{"학번", "이름", "코드", "투표여부"});
	}
	
	public void add(Voter voter) {
		for (int i=0; i<getSize(); i++) {
			if (voter.getNumber().equals(get(i).getNumber())) {
				System.out.println("동일한 Number를 가진 유권자는 추가할 수 없습니다.");
				return;
			}
		}
		super.addRow(new Object[]{voter.getNumber(), voter.getName(), voter.getCode(), voter.isVoted()});
	}
	
	public void remove(Voter voter) {
		for (int i=0; i<getSize(); i++) {
			if (get(i).getNumber().equals(voter.getNumber())) {
				super.removeRow(i);
				return;
			}
		}
	}
	
	public int getSize() {
		return super.getRowCount();
	}
	
	public Voter get(int index) {
		Voter voter =  new Voter(super.getValueAt(index, 0)+"", super.getValueAt(index, 1)+"", super.getValueAt(index, 2)+"");
		if (Boolean.parseBoolean(super.getValueAt(index, 3)+"")) {
			voter.setVoted();
		}
		return voter;
	}
}