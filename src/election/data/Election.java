package election.data;

import java.io.Serializable;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

public class Election implements Serializable {
	public static String title = null;
	public static ImageIcon logo = null;
	public static String ip = null;
	public static final int PORT = 20000;
	public static CandidatorTableModel candidatorList = new CandidatorTableModel();
	public static VoterTableModel voterList = new VoterTableModel();
	public static final Candidator ABSTENTION = new Candidator(0, "기권", null);
	public static final Voter LOGIN_ERROR = new Voter("error", "error", "error");
}