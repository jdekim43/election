package election.data;

import java.io.Serializable;

public class Voter implements Serializable {
	private String number = null;
	private String name = null;
	private String code = null;
	private boolean isVoted = false;
	
	public Voter(String number, String name, String code) {
		this.number = number;
		this.name = name;
		this.code = code;
		System.out.println(number+" voter "+this);
	}
	
	public boolean isValid(String id, String pw) {
		if (number.equals(id) && code.equals(pw)) return true;
		return false;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setVoted() {
		this.isVoted = true;
		System.out.println(number+" setvoted "+this.isVoted+" "+this);
	}
	
	public boolean isVoted() {
		System.out.println(number+" isVoted "+this.isVoted+" "+this);
		return this.isVoted;
	}
	/*
	@Override
	public boolean equals(Object object) {
		Voter voter = null;
		try {
			voter = (Voter)object;
		} catch(ClassCastException e) {
			return false;
		}
		if (voter.getNumber() == null || voter.getName() == null || voter.getCode() == null) return false;
		if (number.equals(voter.getNumber()) && name.equals(voter.getName()) && code.equals(voter.getCode())) {
			return true;
		}
		return false;
	}
	*/
}