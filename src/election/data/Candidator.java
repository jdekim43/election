package election.data;

import java.io.Serializable;

import javax.swing.ImageIcon;

public class Candidator implements Serializable {
	public int num = 0;
	public String name = null;
	public ImageIcon pledge = null;
	
	public Candidator(int num, String name, ImageIcon pledge) {
		this.num = num;
		this.name = name;
		this.pledge = pledge;
	}
}