package election.voter.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import election.data.Election;
import election.voter.Main;

public class Login extends JFrame {
	
	private Callbacks callbacks = null;
	
	private JLabel title = null;
	private JLabel logo = null;
	private TextField inputId = null;
	private TextField inputPw = null;
	private JButton loginBtn = null;
	
	public Login(Main evote) {
		setView();
		callbacks = (Callbacks)evote;
	}
	
	public void open() {
		inputId.setEditable(true);
		inputPw.setEditable(true);
		loginBtn.setEnabled(true);
		setVisible(true);
		setTitle(Election.title, Election.logo);
	}
	
	public void close() {
		inputId.setText("");
		inputPw.setText("");
		setVisible(false);
	}
	
	public void doingLogin() {
		inputId.setEditable(false);
		inputPw.setEditable(false);
		loginBtn.setEnabled(false);
	}
	
	public void failLogin() {
		System.out.println("failLogin");
		JOptionPane.showMessageDialog(null, "등록되지 않은 정보이거나 이미 투표를 하셨습니다.");
		inputId.setEditable(true);
		inputPw.setEditable(true);
		loginBtn.setEnabled(true);
	}
	
	public void setTitle(String title, ImageIcon logo) {
		this.title.setText(title);
		this.logo.setIcon(logo);
	}
	
	private void setView() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(screenSize.width, screenSize.height);
		setUndecorated(true);
		setResizable(false);
		//setAlwaysOnTop(true);
		setBackground(Color.white);
		getRootPane().putClientProperty("apple.awt.draggableWindowBackground", false);
		
		title = new JLabel();
		logo = new JLabel();
		getContentPane().add(title);
		getContentPane().add(logo);
		
		getContentPane().setLayout(new GridBagLayout());
		JPanel loginForm = new JPanel();
		JPanel inputForm = new JPanel();
		JPanel idForm = new JPanel();
		JPanel pwForm = new JPanel();
		loginBtn = new JButton("인증");
		
		loginForm.setLayout(new BorderLayout(3, 3));
		inputForm.setLayout(new GridLayout(2, 2, 3, 3));
		idForm.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));
		pwForm.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));
		
		JLabel idLabel = new JLabel("학번 : ");
		JLabel pwLabel = new JLabel("코드 : ");
		inputId = new TextField();
		inputPw = new TextField();
		
		idLabel.setPreferredSize(new Dimension(40, 30));
		pwLabel.setPreferredSize(new Dimension(40, 30));
		inputId.setPreferredSize(new Dimension(200, 30));
		inputPw.setPreferredSize(new Dimension(200, 30));
		
		loginBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callbacks.login(inputId.getText(), inputPw.getText());
			}
		});
		
		idForm.add(idLabel);
		idForm.add(inputId);
		pwForm.add(pwLabel);
		pwForm.add(inputPw);
		
		inputForm.add(idForm);
		inputForm.add(pwForm);
		loginForm.add(inputForm, BorderLayout.CENTER);
		loginForm.add(loginBtn, BorderLayout.EAST);
		getContentPane().add(loginForm);
		
		//setSize(800, 600);
	}
	
	public static interface Callbacks {
		void login(String id, String pw);
	}
}