package election.voter.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

import election.data.Candidator;
import election.data.Election;
import election.data.Voter;
import election.voter.Main;

public class Voting extends JFrame {
	
	private Callbacks callbacks = null;
	
	private JLabel title = null;
	private JLabel voterName = null;
	private JLabel candidatorImage = null;
	JList<Candidator> candidatorView = null;
	
	private Voter voter = null;
	
	public Voting(Main evote) {
		setView();
		callbacks = (Callbacks)evote;
	}
	
	public void open(Voter voter) {
		if (voter.isVoted()) {
			JOptionPane.showMessageDialog(this, "이미 투표를 하셨습니다.");
			callbacks.voting(voter, null);
			return;
		}
		this.voter = voter;
		title.setText(Election.title);
		voterName.setText("유권자 이름 : "+voter.getName());
		candidatorView.setSelectedIndex(0);
		setVisible(true);
	}
	
	public void close() {
		this.voter = null;
		setVisible(false);
	}
	
	private void setView() {
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setUndecorated(true);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout(4, 4));
		
		/*Election Title*/
		title = new JLabel("전자선거 프로그램");
		getContentPane().add(title, BorderLayout.NORTH);
		
		/*Candidator List*/
		candidatorView = new JList<Candidator>();
		candidatorView.setModel(Election.candidatorList.getCandidatorListModel());
		candidatorView.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		candidatorView.setCellRenderer(new ListCellRenderer<Candidator>() {

			@Override
			public Component getListCellRendererComponent(
					JList<? extends Candidator> list, Candidator value,
					int index, boolean isSelected, boolean cellHasFocus) {
				JPanel box = new JPanel();
				JLabel item = new JLabel(value.name);
				item.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
				if (isSelected) {
					box.setBackground(Color.black);
					//box.setForeground(Color.black);
					item.setForeground(Color.white);
					candidatorImage.setIcon(value.pledge);
				}
				else {
					box.setBackground(Color.white);
					item.setForeground(Color.black);
				}
				box.add(item);
				return box;
			}
		});
		getContentPane().add(new JScrollPane(candidatorView), BorderLayout.WEST);
		
		/*Candidator Detail View*/
		JPanel detailView = new JPanel();
		detailView.setLayout(new GridLayout(0, 1, 0, 0));
		candidatorImage = new JLabel();
		detailView.add(candidatorImage);
		getContentPane().add(detailView, BorderLayout.CENTER);
		
		/*Action Button and Voter Information view*/
		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(1, 4, 4, 4));
		
		JPanel voterInformBox = new JPanel();
		voterName = new JLabel("유권자 이름 : ");
		voterInformBox.add(voterName);
		bottom.add(voterInformBox);
		bottom.add(new JLabel(""));
		JButton abstention = new JButton("기권");
		abstention.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callbacks.voting(voter, Election.ABSTENTION);
			}
		});
		bottom.add(abstention);
		JButton vote = new JButton("투표");
		vote.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callbacks.voting(voter, candidatorView.getSelectedValue());
			}
		});
		bottom.add(vote);
		
		getContentPane().add(bottom, BorderLayout.SOUTH);
		
		
		//setSize(800, 600);
	}

	public static interface Callbacks {
		void voting(Voter voter, Candidator sel);
	}
}