package election.voter;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import election.data.Candidator;
import election.data.Election;
import election.data.Voter;
import election.util.Form;
import election.util.FormCreator;
import election.voter.thread.Receiver;
import election.voter.view.Login;
import election.voter.view.Voting;

public class Main implements Login.Callbacks, Voting.Callbacks, Receiver.Callbacks {
	
	private static Login loginView = null;
	private static Voting votingView = null;
	
	private Receiver receiver = null;
	private ObjectOutputStream sender = null;
	
	//TODO: votedList is admin's variable
	public static void main(String[] args) {
		Main main = new Main();
		main.start();
	}
	
	public Main() {
		connect();
		init();
	}
	
	public void start() {
		loginView = new Login(this);
		votingView = new Voting(this);
		loginView.open();
	}
	
	private void connect() {
		if (Election.ip == null) {
			//Election.ip = JOptionPane.showInputDialog("서버의 IP 주소를 입력하세요.");
			Election.ip = "127.0.0.1";
			connect();
			return;
		}
		receiver = new Receiver(this, Election.ip, Election.PORT);
		receiver.start();
	}
	
	private void init() {
		send(FormCreator.requestData("election_info"));
	}
	
	private void checkUser(String id, String pw) {
		send(FormCreator.login(id, pw));
		loginView.doingLogin();
	}
	
	private void send(Form form) {
		if (sender != null) {
			try {
				sender.writeObject(form);
				sender.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		System.out.println("msg send error");
	}

	@Override
	public void voting(Voter voter, Candidator sel) {
		if (sel!=null) {
			send(FormCreator.vote(voter, sel));
		}
		else if (sel == Election.ABSTENTION){
			send(FormCreator.vote(voter, Election.ABSTENTION));
		}
		loginView.open();
		votingView.close();
	}

	@Override
	public void login(String id, String pw) {
		if (id.equals("admin") && pw.equals("quit")) {
			int ans = JOptionPane.showConfirmDialog(null, "투표 프로그램을 종료하시겠습니까?");
			if (ans == JOptionPane.OK_OPTION) {
				send(FormCreator.quit());
				System.exit(0);
			}
			return;
		}
		checkUser(id, pw);
	}

	@Override
	public void setSender(ObjectOutputStream sender) {
		this.sender = sender;
	}

	@Override
	public void checkedUser(Voter voter) {
		if (voter.getNumber().equals("error")) {
			System.out.println("call failLogin");
			loginView.failLogin();
		}
		else {
			votingView.open(voter);
			loginView.close();
		}
	}

	@Override
	public void setElectionInfo(String title, ImageIcon logo) {
		Election.title = title;
		Election.logo = logo;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void setCandidatorList(Vector candidatorList) {
		for (int i=0; i<candidatorList.size(); i++) {
			Election.candidatorList.addRow((Vector)candidatorList.get(i));
		}
	}
}