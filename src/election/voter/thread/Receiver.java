package election.voter.thread;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ImageIcon;

import election.data.Voter;
import election.util.Form;
import election.util.MessageCreator;
import election.voter.Main;

public class Receiver extends Thread {
	
	private Socket socket = null;
	private ObjectInputStream receiver = null;
	private Callbacks callbacks = null;
	
	public Receiver(Main election, String ip, int port) {
		callbacks = (Callbacks)election;
		try {
			socket = new Socket(ip, port);
			callbacks.setSender(new ObjectOutputStream(socket.getOutputStream()));
			receiver = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void run() {
		Object in = null;
		while (true) {
			try {
				in = receiver.readObject();
				System.out.println(in);
			} catch(IOException e) {
				//e.printStackTrace();
			} catch(ClassNotFoundException e) {
				//e.printStackTrace();
			}
			if (in == null) {
				continue;
			}
			Form input = null;
			try {
				input = (Form)in;
			} catch(ClassCastException e) {
				e.printStackTrace();
			}
			String type = input.type;
			HashMap<String, Object> data = input.data;
			if (type.equals(MessageCreator.LOGIN)) {
				callbacks.checkedUser((Voter)data.get("voter"));
			}
			else if (type.equals(MessageCreator.ELECTION_INFO)) {
				callbacks.setElectionInfo((String)data.get("title"), (ImageIcon)data.get("logo"));
				callbacks.setCandidatorList((Vector)data.get("candidator_list"));
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public static interface Callbacks {
		void setSender(ObjectOutputStream sender);
		void checkedUser(Voter voter);
		void setElectionInfo(String title, ImageIcon logo);
		void setCandidatorList(Vector candidatorList);
	}
}